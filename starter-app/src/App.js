import React from 'react';
import logo from './logo.svg';
import {useState} from 'react';
import './App.css';

function App() {

  const [todo, setTodo] = useState([]) 

  const [addVal, setAddVal] = useState("");

  function addTodo(){
    if(addVal!=""){
    setTodo(todo.concat(addVal));
    setAddVal("");
    }
  }

  return (
    <div>
        <h1>//TODO</h1>
        <add>
          <input placeholder="text" onKeyPress={(event) => {if(event.key === 'Enter'){addTodo()}}} onChange={e => setAddVal(e.target.value)} value={addVal}></input>
          <input type="button" value="Add" onClick={() => {addTodo()}}></input>
        </add>
        <ul>
          {todo.map((item, index) => <div class="item"><li>{item}<x onClick={() => {setTodo(todo.filter((cur,i) => i!==index))}}>X</x></li></div>)}
        </ul>
    </div>
  );
}

export default App;
